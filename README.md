![logo](media/logo.png "Logo")

---

![pipeline](https://img.shields.io/gitlab/pipeline/gruberx/insomnia-documenter-for-gitlab?style=flat-square) ![status](https://img.shields.io/website?label=docs&style=flat-square&up_message=online&url=https%3A%2F%2Fgruberx.gitlab.io%2Finsomnia-documenter-for-gitlab) ![mantained](https://img.shields.io/maintenance/yes/2020?style=flat-square)

![screenshot](media/website.png)


An example on how to use Insomnia with Gitlab CI/CD for auto-gen. documentation

Learn more about [GitLab Pages](https://about.gitlab.com/product/pages), [Insomnia Core](https://github.com/Kong/insomnia) and [Insomnia Design](https://insomnia.rest/download/).

This project also uses [Swaggomania](https://github.com/Fyb3roptik/swaggomnia), a fork of [swaggymnia](https://github.com/mlabouardy/swaggymnia) which is deprecated for now.


## GitLab CI

This project's static Pages are built by GitLab CI, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
pages:
  image: node:14
  stage: deploy
  script:
    - npm install -q -g redoc-cli
    - wget https://github.com/Fyb3roptik/swaggomnia/releases/download/2.0.1/swaggomnia_linux_amd64.tar.gz
    - tar -xzf swaggomnia_linux_amd64.tar.gz
    - mv swaggomnia_linux_amd64/swaggomnia swaggomania
    - chmod +x swaggomania
    - ./swaggomania generate --insomnia insomnia-export.json -config config.json
    - redoc-cli bundle -o index.html swagger.yaml
    - mkdir .public 
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
  - master
```

* First, `redoc-cli` is installed globally inside the Docker's image
* Downloads the latest release of *Swaggomania* from their Github's repository
* Bundles your exported insomnia file `insomnia-export.json` with the configuration file inside a single html
* Exposes the output to Gitlab Pages

## Building your own Documentation

There are two files needed for the pipeline, `config.json` has to be configured manually, per project basis:

```json
{
    "title" : "Swagger Petstore",
    "version" : "1.0.2",
    "basePath" : "https://petstore.swagger.io/v2",
    "description" : "A petshop example API"
}
```

And `insomnia-export.json` is a result from importing the OpenAPI v3.0 `petstore-spec.yaml` example file in *Insomnia's Core/Designer* and exporting it out as a `Insomnia v4 (JSON)` file.

This can be done by going to:

* **Application** > **Preferences** > **Data** > **Export Data** > **Current Workspace** or **All Workspaces** and finally selecting **Insomnia V4 (JSON)** and clicking **export**. The example spec used here is the same as the default example file in Insomnia's Design, but it's included as well in this repository so you can learn importing a different format inside Insomnia and then exporting. 

When the pipeline finishes executing, it'll be available in the following domain:
`https://your-username.gitlab.io/{your_repository_name}`

* e.g: This repository is hosted at [gruberx.gitlab.io/insomnia-documenter-for-gitlab](https://gruberx.gitlab.io/insomnia-documenter-for-gitlab).

## Did you fork this project?

**Remember to**:

* Delete `petstore-spec.yaml` if unecessary.
* Export your own project and adjust Gitlab's CI/CD file `gitlab-ci.yml` parameters to execute your `*.json` file instead of the example file `insomnia-export.json`.
* Remove the fork link as it not needed for your own project, this is just a showcase.

## Bugs/Issues/Update issues

Feel free to open an issue for that. 
